package com.exsoloscript.nohorse.timer;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;

import com.exsoloscript.nohorse.NoHorse;

public class HorseButcherTimer implements Runnable {

	private NoHorse plugin;
	
	public HorseButcherTimer(NoHorse n) {
		this.plugin = n;
	}
	
	public void run() {
		if (this.plugin.getConfig().getBoolean("config.spawns") == false) {
			for (World w : Bukkit.getWorlds()) {
				for (Entity e : w.getEntitiesByClass(Horse.class)) {
					e.remove();
				}
			}
		}
	}

}
