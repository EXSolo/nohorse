package com.exsoloscript.nohorse.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;

import com.exsoloscript.nohorse.NoHorse;

public class HorseListener implements Listener {

	private NoHorse plugin;

	public HorseListener(NoHorse n) {
		this.plugin = n;
	}

	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if (plugin.getConfig().getBoolean("config.spawns") != true) {
			if (event.getEntityType() == EntityType.HORSE)
				event.setCancelled(true);
		}
	}

	@EventHandler
	public void onVehicleEnter(VehicleEnterEvent event) {
		if (plugin.getConfig().getBoolean("config.explosion") == true) {
			if (event.getVehicle().getType() == EntityType.HORSE) {
				event.getVehicle().getWorld().createExplosion(event.getVehicle().getLocation(), 8);
			}
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (plugin.getConfig().getBoolean("config.explosion") == true) {
			if (event.getWhoClicked() instanceof Player)
				if (event.getCurrentItem().getType() == Material.SADDLE) {
					((Player) event.getWhoClicked()).sendMessage(this.plugin.prefix() + ChatColor.DARK_RED + "Horses are disabled, don't use them!");
				}
		}

		if (plugin.getConfig().getBoolean("config.armor") != true) {
			if (event.getWhoClicked() instanceof Player) {
				Material m = event.getCurrentItem().getType();
				if (m == Material.DIAMOND_BARDING || m == Material.GOLD_BARDING || m == Material.IRON_BARDING) {
					Player p = ((Player) event.getWhoClicked());
					p.sendMessage(plugin.prefix() + ChatColor.DARK_RED + "Horse armor is disabled.");
					if (!p.hasPermission("nohorse.armor")) {
						event.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (plugin.getConfig().getBoolean("config.healing") == false) {
			if (event.getRightClicked().getType() == EntityType.HORSE) {
				Material type = event.getPlayer().getItemInHand().getType();
				if (type == Material.BREAD || type == Material.APPLE || type == Material.SUGAR || type == Material.WHEAT || type == Material.GOLDEN_APPLE || type == Material.GOLDEN_CARROT || type == Material.HAY_BLOCK) {
					event.getPlayer().setItemInHand(null);
					event.getPlayer().sendMessage(this.plugin.prefix() + ChatColor.DARK_RED + "Horse healing is disabled.");
					event.setCancelled(true);
				}
			}
		}
	}

}
