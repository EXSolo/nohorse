package com.exsoloscript.nohorse.cmd;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.exsoloscript.nohorse.NoHorse;

public class ConfigCommand implements CommandExecutor {

	private NoHorse plugin;
	private Set<String> features;

	public ConfigCommand(NoHorse n) {
		this.plugin = n;
		this.features = this.plugin.getConfig().getConfigurationSection("config").getKeys(true);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("horseconfig") || label.equalsIgnoreCase("hc")) {
			if (sender.hasPermission("nohorse.toggle")) {
				if (args.length > 0) {
					if (args.length > 1) {
						if (args[0].equalsIgnoreCase("toggle")) {
							if (this.features.contains(args[1])) {
								boolean featureState = this.plugin.getConfig().getBoolean("config." + args[1]);

								this.plugin.getConfig().set("config." + args[1], !featureState);
								this.plugin.saveConfig();
								Bukkit.broadcastMessage(plugin.prefix() + "Feature '" + args[1] + "' was set to " + !featureState);
							} else {
								sender.sendMessage(plugin.prefix() + "Unknown feature");
							}
						}
						return true;
					}

					if (args[0].equalsIgnoreCase("features")) {
						sender.sendMessage(plugin.prefix() + "Listing features");

						for (String s : this.features) {
							sender.sendMessage(" - " + s + ": " + this.plugin.getConfig().getBoolean("config." + s));
						}
					} else {
						sender.sendMessage(plugin.prefix() + "Correct usage: /horseconfig <toggle;features> [feature]");
					}
				} else {
					sender.sendMessage(plugin.prefix() + "Correct usage: /horseconfig <toggle;features> [feature]");
				}
			}
		}
		
		return false;
	}

}
