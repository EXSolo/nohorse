package com.exsoloscript.nohorse;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.exsoloscript.nohorse.cmd.ConfigCommand;
import com.exsoloscript.nohorse.listener.HorseListener;
import com.exsoloscript.nohorse.timer.HorseButcherTimer;

public class NoHorse extends JavaPlugin implements Listener {

	public void onEnable() {
		register();
	}
	
	public void register() {
		this.getConfig().addDefault("config.explosion", false);
		this.getConfig().addDefault("config.spawns", true);
		this.getConfig().addDefault("config.healing", true);
		this.getConfig().addDefault("config.armor", true);
		
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		this.getCommand("horseconfig").setExecutor(new ConfigCommand(this));
		
		Bukkit.getPluginManager().registerEvents(new HorseListener(this), this);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new HorseButcherTimer(this), 0, 1200);
	}
	
	public String prefix() {
		return ChatColor.AQUA + "[" + ChatColor.GOLD + "NoHorse" + ChatColor.AQUA + "] " + ChatColor.RESET;
	}
}
